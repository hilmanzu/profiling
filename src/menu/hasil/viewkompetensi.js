import React, { Component } from 'react';
import { Button } from 'reactstrap';
import firebase from 'firebase'
import swal from 'sweetalert';
var ReactTable = require("react-table").default;

class bonus extends Component {

    constructor(props){
        super(props);
        this.state = {
          member :[],
          id     :'',
          nomor  :''
        }  
      }

     componentWillMount(){
       const db = firebase.firestore();
       const docRef = db.collection("kompetens")
       docRef.onSnapshot(async(querySnapshot)=>{
        var data = []
        querySnapshot.forEach((doc)=>{
          let datas = data
          datas.push({
            id   : doc.id,
            data : doc.data()
          })
        })
        this.setState({
          member    : data,
          nomor     : Object.keys(data).length
        })
       
       })
    }

  render() {
    const {member} = this.state;
    const columns = [{
                      Header: 'No',
                      id: 'no',
                      width: 40,
                      style: {textAlign: 'center'},
                      Cell: (row) => {return <div>{row.index+1}</div>}
                    },{
                      Header: 'Nama Lengkap',
                      id: 'nama',
                      accessor: d => 
                        <div style={{textAlign:'center'}}>
                          <td ><p>{d.data.name_model}</p></td>
                        </div>
                    },{
                      id: 'detail',
                      Header: 'Detail',
                      width: 150,
                      accessor: d => 
                        <Button color="success" style={{width:100,marginLeft:20}} 
                          onClick={()=>{
                            localStorage.setItem('cluster',d.id)
                            this.props.history.push('/Viewuser')}}>
                          Lihat
                        </Button>
                    }]

    return (
      <div>
       <ReactTable
          data={member}
          columns={columns}
          defaultPageSize={10}
          className="-striped -highlight"
          noDataText="Data Tidak Ditemukan"
        />
      </div>
    )
  }

  tambah=()=>{
   this.props.history.push('/edit/tambahmember')
  }

}

export default bonus;
