import React, { Component } from 'react';
import firebase from 'firebase'
import swal from 'sweetalert';
import ReactTable from 'react-table'
import { Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle, Button } from 'reactstrap';
import '../hasil/card.css'
import renderIf from './renderIf'
import Chart from "react-apexcharts";

const gambar = "https://centrik.in/wp-content/uploads/2017/02/user-image-.png"

class bonus extends Component {

    constructor(props){
        super(props);
        this.state = {
          member :[],
          nomor  :'',
          loading:true,
          profile:'',
          req :'',
          options:[],
          seriess:[],
          jabatan:''
        }  
      }

    componentDidMount(){
      var id      = localStorage.getItem('id');
      var jabatan = localStorage.getItem('jabatan');
      var model   = localStorage.getItem('model');

      const db = firebase.firestore();
      const docRef = db.collection("user").doc(id).collection('Threshold')
      const items = db.collection("user").doc(id)
      const jabatans = db.collection("kompetens").doc(model).collection('jabatan').doc(jabatan)

      items.onSnapshot( async (doc) => {
        let item = doc.data()
        this.setState({ profile:item,loading:false })
      })

      jabatans.onSnapshot( async (doc) => {
          let data = doc.data()
          this.setState({ jabatan : data })
      })

       docRef.onSnapshot(async(querySnapshot)=>{
        var data = []
        querySnapshot.forEach((doc)=>{
          let datas = data
          datas.push({
            id   : doc.id,
            data : doc.data()
          })
        })
        this.setState({
          member    : data,
          options   : data.map((data)=> data.data.nama),
          seriess   : data.map((data)=> data.data.nilai),
          req       : data.map((data)=> data.data.req),
          loading   : false
        })
       
       })
    }

  render() {
    const {loading,profile,options,seriess,req,jabatan} = this.state;
    console.log(options)
    console.log(seriess)
    const option = 
        {
          chart: {
            dropShadow: {
              enabled: true,
              blur: 1,
              left: 1,
              top: 1
            },
          },
          labels: options,
          title: {
            text: 'Penilaian'
          },
          stroke: {
            width: 0
          },
          fill: {
            opacity: 0.4
          },
          markers: {
            size: 0
          }
        }

    const series = 
        [{
            name: "Nilai Kompetensi",
            data: seriess
        },{
            name: "Reqruitmen",
            data: req
        }]

    if (loading === true) {
      return(
        <div>
        </div>
      )
    }

    return (
          <div class='rowchard'>
            <Card style={{paddingLeft:30,paddingRight:20,paddingTop:20,paddingBottom:10}}>
              <div style={{marginBottom:20}}>
                <td>
                  <p>Nama User</p>
                  <p>NIP</p>
                </td>
                <td>
                  <p> :  {profile.firstname} {profile.lastname}</p>
                  <p> :  {profile.nip}</p>
                </td>
                <td>
                  <p style={{marginLeft:100}}>Lokasi</p>
                  <p style={{marginLeft:100}}>Jabatan</p>
                </td>
                <td>
                  <p> :  {profile.wilayah}</p>
                  <p> :  {jabatan.jabatan}</p>
                </td>
              </div>
              <Chart
                options={option}
                series={series}
                type="bar" 
                height="400"
                width="400"
              />
          </Card>
        </div>
    )
  }

  tambah=()=>{
   this.props.history.push('/edit/tambahmember')
  }

}

export default bonus;
