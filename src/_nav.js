export default {
  items: [
    {
      name: 'Biodata Saya',
      icon: 'icon-user',
    },
    {
      name: 'Pengguna',
      url: '/Pengguna',
      icon: 'icon-people',
    },
    {
      name: 'Hasil',
      url: '/Viewuser',
      icon: 'icon-docs',
    },
    {
      name: 'Keluar',
      url: '/Keluar',
      icon: 'icon-logout',
    }
  ],
};
